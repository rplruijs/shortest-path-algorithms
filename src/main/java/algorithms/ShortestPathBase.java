package algorithms;

import java.util.Comparator;
import java.util.List;

import modelsabstractions.GameSquare2D;
import modelsabstractions.GraphNode;
import modelsabstractions.INode;
import modelsabstractions.IPath;
import modelsimplementation.Direction;

public abstract class ShortestPathBase implements ShortestPathAlgorithm {

    protected ShortestPathWorker shortestPathWorker;

    public ShortestPathBase() {
        this.shortestPathWorker = new ShortestPathWorker(getNodeComparator());
    }

    abstract Comparator<INode> getNodeComparator();

    public IPath shortestPath(GraphNode startNode, GraphNode endNode) throws NoPathPossibleException{
        return shortestPathWorker.shortestPath(startNode, endNode);
    }

    public List<Direction> directionsToMove(GameSquare2D startNode, GameSquare2D endNode) throws NoPathPossibleException, StartAndEndAreThereSameException{
        return shortestPathWorker.directionsToMove(startNode, endNode);
    }


}
