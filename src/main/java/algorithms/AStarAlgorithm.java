package algorithms;

import java.util.Comparator;

import modelsabstractions.GraphNode;
import modelsabstractions.INode;


/***
 * @author Remco Ruijsenaars
 */
public class AStarAlgorithm extends ShortestPathBase {

    private GraphNode endNode;

    public AStarAlgorithm(GraphNode endNode) {
        this.endNode = endNode;
    }


    @Override
    protected Comparator<INode> getNodeComparator() {
        return (t1, t2) -> {

            double korsteAfstand1 = super.shortestPathWorker.kortseAfstandenTotStartCel.get(t1) + INode.manhattanDistance(t1, endNode);
            double kortsteAfstand2 = super.shortestPathWorker.kortseAfstandenTotStartCel.get(t2) + INode.manhattanDistance(t2, endNode);

            return (int) Math.signum(korsteAfstand1 - kortsteAfstand2);

        };
    }

}
