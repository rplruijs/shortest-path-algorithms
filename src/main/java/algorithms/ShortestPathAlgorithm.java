package algorithms;


import java.util.List;

import modelsabstractions.GameSquare2D;
import modelsabstractions.GraphNode;
import modelsabstractions.IPath;
import modelsimplementation.Direction;

/**
 * @author Remco Ruijsenaars
 */

public interface ShortestPathAlgorithm {

    IPath shortestPath(GraphNode startNode, GraphNode endNode) throws NoPathPossibleException;

    List<Direction> directionsToMove(GameSquare2D startNode, GameSquare2D endNode) throws NoPathPossibleException, StartAndEndAreThereSameException;


}
