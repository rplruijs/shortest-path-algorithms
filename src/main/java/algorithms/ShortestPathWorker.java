package algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import adapters.GameSquare2DAdapter;
import javaslang.Tuple4;
import modelsabstractions.GameSquare2D;
import modelsabstractions.GraphNode;
import modelsabstractions.INode;
import modelsabstractions.IPath;
import modelsimplementation.Direction;
import modelsimplementation.Path;

/**
 *
 * @author Remco Ruijsenaars
 */

final public class ShortestPathWorker {

    protected Map<GraphNode, Double> kortseAfstandenTotStartCel = new HashMap<GraphNode, Double>();
    private Map<GraphNode, GraphNode> predecessors= new HashMap<GraphNode, GraphNode>();
    private Set<GraphNode> closed = new HashSet<GraphNode>();

    private Comparator<INode> comparator;
    private PriorityQueue<GraphNode> open;


    public ShortestPathWorker(Comparator<INode> comparator){
        this.comparator = comparator;
        setUpDatastructures();
    }

    private void start(GraphNode startNode, GraphNode endNode){
        init(startNode);
        startAlgorithm(endNode);
    }

    private void setUpDatastructures(){
        kortseAfstandenTotStartCel = new HashMap<GraphNode, Double>();
        predecessors= new HashMap<GraphNode, GraphNode>();
        closed = new HashSet<GraphNode>();
        open = new PriorityQueue<GraphNode>(1, this.comparator);
    }


    private IPath creeerPath(GraphNode startNode, GraphNode endNode){

        List<GraphNode> pad = new ArrayList<GraphNode>();
        pad.add(endNode);

        boolean startCelGevonden = false;
        GraphNode zoekNode = endNode;
        while(!startCelGevonden){
            GraphNode celIter = predecessors.get(zoekNode);
            pad.add(celIter);
            zoekNode = celIter;
            if(celIter.equals(startNode)){
                startCelGevonden = true;
            }
        }

        Collections.reverse(pad);

        Path path = new Path();
        path.setPath(pad);
        path.setCosts(this.kortseAfstandenTotStartCel.get(endNode));

        return path;
    }


    private void startAlgorithm(GraphNode endNode){
        while(!open.isEmpty() && !closed.contains(endNode)){
            GraphNode cel = extractMinimum();
            closed.add(cel);
            relaxeNeighbours(cel);
        }
    }

    private void init(GraphNode startCel){
        kortseAfstandenTotStartCel.put(startCel, new Double(0));
        open.add(startCel);
    }

    private GraphNode extractMinimum(){
        GraphNode cel = this.open.poll();
        return cel;
    }

    private void relaxeNeighbours(GraphNode cel){
        Map<GraphNode, Double> buren = cel.neighboursWithDistances();
        Set keys = buren.keySet();
        Iterator<GraphNode> iterator =  keys.iterator();

        while(iterator.hasNext()){
            GraphNode celBuur = iterator.next();
            if(!closed.contains(celBuur)){
                double kortsteAfstandTotCel = kortseAfstandenTotStartCel.get(cel);
                double kortsteafstandTotcelBuur = Integer.MAX_VALUE;
                if(kortseAfstandenTotStartCel.containsKey(celBuur)){
                    kortsteafstandTotcelBuur = kortseAfstandenTotStartCel.get(celBuur);
                }

                double afstand = buren.get(celBuur);

                if(kortsteafstandTotcelBuur > kortsteAfstandTotCel  + afstand){
                    kortseAfstandenTotStartCel.put(celBuur, new Double(kortsteAfstandTotCel + afstand));
                    predecessors.put(celBuur, cel);
                    open.add(celBuur);
                }
            }
        }
    }

    private IPath getShortestPath(GraphNode startNode, GraphNode endNode) throws NoPathPossibleException{
        Iterator<GraphNode> iterator = predecessors.keySet().iterator();
        while(iterator.hasNext()){
            GraphNode iterNode = iterator.next();
            if(iterNode!=null){
                if(iterNode == endNode){
                    return creeerPath(startNode, iterNode);
                }
            }

        }
        throw new NoPathPossibleException();
    }

    public IPath shortestPath(GraphNode startNode, GraphNode endNode) throws NoPathPossibleException {
        start(startNode, endNode);
        return getShortestPath(startNode, endNode);
    }


    public List<Direction> directionsToMove(GameSquare2D startNode, GameSquare2D endNode) throws NoPathPossibleException, StartAndEndAreThereSameException {

        if(startNode.equals(endNode)){
            throw new StartAndEndAreThereSameException();
        }

        GameSquare2DAdapter adapter = new GameSquare2DAdapter();
        final Tuple4<GraphNode, GraphNode, List<GameSquare2D>, List<GraphNode>> convert = adapter.convert(startNode, endNode);
        start(convert._1, convert._2);
        final IPath shortestPath = getShortestPath(convert._1, convert._2);

        GraphNode from = shortestPath.getPath().get(0);

        final List<Direction> directions = new ArrayList<>();
        for (int i = 1; i < shortestPath.getPath().size(); i++) {
            GraphNode to = shortestPath.getPath().get(i);
            Direction direction = fromGraphNodeToDirection(from, to, convert._3, convert._4);
            directions.add(direction);
            from = to;
        }

        return directions;
    }

    private Direction fromGraphNodeToDirection(GraphNode from, GraphNode to, List<GameSquare2D> gameSquares, List<GraphNode> graphNodes) throws NoPathPossibleException {
        GameSquare2D gsFrom = gameSquares.get(graphNodes.indexOf(from));
        GameSquare2D gsEnd = gameSquares.get(graphNodes.indexOf(to));

        for (Map.Entry<Direction, GameSquare2D> entry : gsFrom.neighboursWithDirections().entrySet()) {

            if(entry.getValue().equals(gsEnd)){
                return entry.getKey();
            }
        }

        throw new NoPathPossibleException();
    }

}
