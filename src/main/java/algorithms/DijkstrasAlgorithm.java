package algorithms;

import modelsabstractions.INode;

import java.util.*;

/**
 * @author Remco Ruijsenaars
 */
public class DijkstrasAlgorithm extends ShortestPathBase {


    public Comparator<INode> getNodeComparator() {
        return (t, t1) -> {

            double korsteAfstand1 = super.shortestPathWorker.kortseAfstandenTotStartCel.get(t);
            double kortsteAfstand2 = super.shortestPathWorker.kortseAfstandenTotStartCel.get(t1);

            return (int) Math.signum(korsteAfstand1 - kortsteAfstand2);
        };
    }

}
