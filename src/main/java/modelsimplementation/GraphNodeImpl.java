package modelsimplementation;

import modelsabstractions.GraphNode;
import java.util.HashMap;
import java.util.Map;

public class GraphNodeImpl implements GraphNode {

    private double X;
    private double Y;
    private String name;

    private Map<GraphNode, Double> neighbours = new HashMap<>();

    private GraphNodeImpl(double x, double y, String name) {
        X = x;
        Y = y;
        this.name = name;
    }

    public static GraphNodeImpl of(double x, double y, String name){
        return new GraphNodeImpl(x, y, name);
    }

    @Override
    public Map<GraphNode, Double> neighboursWithDistances() {
        return neighbours;
    }

    public void setNeighbour(GraphNode node, double distance){
        this.neighbours.put(node, distance);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getX() {
        return this.X;
    }

    @Override
    public double getY() {
        return this.Y;
    }

    @Override public String toString() {
        return "Node{" + "name='" + name + '\'' + '}';
    }
}
