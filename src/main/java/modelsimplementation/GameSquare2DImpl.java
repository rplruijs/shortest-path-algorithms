package modelsimplementation;

import java.util.EnumMap;
import modelsabstractions.GameSquare2D;


public class GameSquare2DImpl implements GameSquare2D{

    private double X;
    private double Y;
    private String name;

    private EnumMap<Direction, GameSquare2D> neighboursWithDirection = new EnumMap<>(Direction.class);

    public GameSquare2DImpl(double x, double y, String name) {
        this.X = x;
        this.Y = y;
        this.name = name;
    }

    @Override
    public EnumMap<Direction, GameSquare2D> neighboursWithDirections() {
        return this.neighboursWithDirection;
    }

    public void setNeighbour(GameSquare2D gameSquare2D, Direction direction){
        this.neighboursWithDirection.put(direction, gameSquare2D);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getX() {
        return this.X;
    }

    @Override
    public double getY() {
        return this.Y;
    }
}
