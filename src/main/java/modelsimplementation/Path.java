package modelsimplementation;

import modelsabstractions.GraphNode;
import modelsabstractions.IPath;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author remcoruijsenaars
 */
public class Path implements IPath {

    private List<GraphNode> path = new ArrayList<GraphNode>();
    private double totalCosts;

    public List<GraphNode> getPath() {
        return path;
    }

    public void setPath(List<GraphNode> path) {
        this.path = path;
    }

    public double getTotalCosts() {
        return totalCosts;
    }

    public void setCosts(double totalCosts) {
        this.totalCosts = totalCosts;
    }

    public String toString(){
        String returner = "";
        for(GraphNode cel: path){
            returner+=cel.toString() + "-->";
        }

        returner+= "\n kosten:" + totalCosts;
        return returner;
    }

}

