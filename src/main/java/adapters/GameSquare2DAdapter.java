package adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import algorithms.NoPathPossibleException;
import javaslang.Tuple4;
import modelsabstractions.GameSquare2D;
import modelsabstractions.GraphNode;
import modelsimplementation.Direction;
import modelsimplementation.GraphNodeImpl;

public class GameSquare2DAdapter {


    final List<GameSquare2D> visited = new ArrayList<>();
    final List<GraphNodeImpl> converted = new ArrayList<>();

    public Tuple4<GraphNode, GraphNode, List<GameSquare2D>, List<GraphNode>> convert(GameSquare2D start, GameSquare2D end) throws NoPathPossibleException {

        traverse(start);
        setNeighsConverted();

        int indexOfStart = visited.indexOf(start);
        int indexOfEnd = visited.indexOf(end);

        if(indexOfEnd<0 || indexOfEnd<0){
            throw new NoPathPossibleException();
        }

        return new Tuple4(converted.get(indexOfStart), converted.get(indexOfEnd), visited, converted);
    }

    private void traverse(GameSquare2D node) {

        if(!visited.contains(node)){
            visited.add(node);
            converted.add(toGraphNode(node));
        }
        for (Map.Entry<Direction, GameSquare2D> entry : node.neighboursWithDirections().entrySet()) {

            if(!visited.contains(entry.getValue())){
                visited.add(entry.getValue());
                converted.add(toGraphNode(entry.getValue()));
            }

            entry.getValue()
                    .neighboursWithDirections()
                    .entrySet()
                    .stream()
                    .map(e -> e.getValue())
                    .filter(g -> !visited.contains(g))
                    .forEach(x -> traverse(x));

        }

    }

    private void setNeighsConverted(){
        for (int i = 0; i < visited.size() ; i++) {

            List<Integer> neighs = positionsNeighs(visited.get(i));

            for (Integer index: neighs) {
                GraphNodeImpl iter = converted.get(i);
                iter.setNeighbour(converted.get(index), 1);
            }
        }
    }


    private List<Integer> positionsNeighs(GameSquare2D gs){
        List<Integer> indicesNeighs = gs.neighboursWithDirections().entrySet()
                .stream()
                .map(e -> e.getValue())
                .map(n -> visited.indexOf(n)).collect(Collectors.toList());


        return indicesNeighs;
    }


    private GraphNodeImpl toGraphNode(GameSquare2D node) {
        return GraphNodeImpl.of(node.getX(), node.getX(), node.getName());
    }
}

