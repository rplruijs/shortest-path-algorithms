package modelsabstractions;

import java.util.List;

/**
 * author remcoruijsenaars.
 */

public interface IPath {
    List<GraphNode> getPath();
    double getTotalCosts();

}
