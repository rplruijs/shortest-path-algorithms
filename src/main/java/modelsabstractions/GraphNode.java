package modelsabstractions;

/**
 *
 * @author Remco Ruijsenaars
 */

import java.util.Map;

public interface GraphNode extends INode {

    Map<GraphNode, Double> neighboursWithDistances();
}
