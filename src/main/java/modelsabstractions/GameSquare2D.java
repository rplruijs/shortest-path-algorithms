package modelsabstractions;

import java.util.EnumMap;

import modelsimplementation.Direction;

public interface GameSquare2D extends INode{
    EnumMap<Direction, GameSquare2D> neighboursWithDirections();
}
