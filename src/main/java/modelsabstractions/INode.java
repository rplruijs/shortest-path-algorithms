package modelsabstractions;

public interface INode {
    String getName();
    double getX(); // latitude
    double getY(); // longitude

    static double manhattanDistance(INode n1, INode n2){
        double xDelta = Math.abs(n1.getX() - n2.getX());
        double yDelta = Math.abs(n1.getY() - n2.getY());

        return xDelta + yDelta;
    }
}
