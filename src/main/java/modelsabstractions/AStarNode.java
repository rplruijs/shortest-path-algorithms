package modelsabstractions;

public interface AStarNode {
    double getX(); // latitude
    double getY(); // longitude
}
