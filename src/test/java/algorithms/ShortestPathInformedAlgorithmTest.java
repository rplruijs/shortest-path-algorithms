package algorithms;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import modelsabstractions.IPath;
import modelsimplementation.GraphNodeImpl;

public class ShortestPathInformedAlgorithmTest {

    private GraphNodeImpl a = null;
    private GraphNodeImpl b = null;
    private GraphNodeImpl c = null;
    private GraphNodeImpl d = null;
    private GraphNodeImpl e = null;
    private GraphNodeImpl f = null;

    private ShortestPathAlgorithm dijkstrasAlgorithm;

    @Before
    public void setUp(){
        a = GraphNodeImpl.of(25, 20, "A");
        b = GraphNodeImpl.of(60, 30, "B");
        c = GraphNodeImpl.of(35, 50, "C");
        d = GraphNodeImpl.of(90, 50, "D");
        e = GraphNodeImpl.of(75, 75, "E");
        f = GraphNodeImpl.of(10, 75, "F");


        //Setting Neighs A
        a.setNeighbour(b, 10);

        //Setting Neighs B
        b.setNeighbour(c, 5);
        b.setNeighbour(d, 6);

        //Setting Neighs C
        c.setNeighbour(f, 8);
        c.setNeighbour(e, 5);
        c.setNeighbour(b, 3);

        //Setting Neighs D
        d.setNeighbour(b, 12);
        d.setNeighbour(e, 1);

        //Setting Neighs E
        e.setNeighbour(d, 3);

        //Setting Neighs F
        f.setNeighbour(e, 4);

        dijkstrasAlgorithm= new DijkstrasAlgorithm();
    }


    @Test
    public void dijkstra_should_return_the_correct_shortestpath_in_valid_graph() throws Exception{


        final IPath shortestPath = dijkstrasAlgorithm.shortestPath(a, e);

        final String expectedPath = "ABDE";

        String realPath = shortestPath
                                .getPath()
                                .stream()
                                .map(n -> n.getName())
                                .reduce("", String::concat);


        Assert.assertTrue(expectedPath.equals(realPath));

    }


    @Test
    public void astar_should_return_the_correct_shortestpath_in_valid_graph() throws Exception{

        ShortestPathAlgorithm aStar = new AStarAlgorithm(e);

        final IPath shortestPath = aStar.shortestPath(a, e);

        final String expectedPath = "ABDE";

        String realPath = shortestPath
                .getPath()
                .stream()
                .map(n -> n.getName())
                .reduce("", String::concat);


        Assert.assertTrue(expectedPath.equals(realPath));

    }

    @Test(expected = NoPathPossibleException.class)
    public void dijkstra_should_return_null_when_no_path_possible_() throws Exception{
        ShortestPathAlgorithm algo = new DijkstrasAlgorithm();

        IPath result = algo.shortestPath(e, a);

    }

    @Test(expected = NoPathPossibleException.class)
    public void astar_should_return_null_when_no_path_possible_() throws Exception{
        ShortestPathAlgorithm algo = new AStarAlgorithm(a);

        IPath result = algo.shortestPath(e, a);
    }



}