package adapters;

import static modelsimplementation.Direction.EAST;
import static modelsimplementation.Direction.SOUTH;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import algorithms.DijkstrasAlgorithm;
import algorithms.NoPathPossibleException;
import algorithms.StartAndEndAreThereSameException;
import javaslang.Tuple4;
import modelsabstractions.GameSquare2D;
import modelsabstractions.GraphNode;
import modelsimplementation.Direction;
import modelsimplementation.GameSquare2DImpl;
import modelsimplementation.GraphNodeImpl;

public class GameSquare2DAdpaperTest {


    private GameSquare2DImpl A;
    private GameSquare2DImpl B;
    private GameSquare2DImpl C;
    private GameSquare2DImpl D;
    private GameSquare2DImpl E;
    private GameSquare2DImpl F;
    private GameSquare2DImpl G;
    private GameSquare2DImpl H;
    private GameSquare2DImpl I;

    private GameSquare2DImpl Z;


    @Before
    public void setup(){
        A = new GameSquare2DImpl(0, 0, "A");
        B = new GameSquare2DImpl(0, 1, "B");
        C = new GameSquare2DImpl(0, 2, "C");
        D = new GameSquare2DImpl(1, 0, "D");
        E = new GameSquare2DImpl(1, 1, "E");
        F = new GameSquare2DImpl(1, 2, "F");
        G = new GameSquare2DImpl(2, 0, "G");
        H = new GameSquare2DImpl(2, 1, "H");
        I = new GameSquare2DImpl(2, 2, "I");

        Z = new GameSquare2DImpl(42, 42, "Z") ;

        //Neighs A
        A.setNeighbour(D, SOUTH);

        //Neighs D
        D.setNeighbour(A, Direction.NORTH);
        D.setNeighbour(E, EAST);

        //Neighs E
        E.setNeighbour(D, Direction.WEST);
        E.setNeighbour(B, Direction.NORTH);
        E.setNeighbour(H, SOUTH);

        //NEIGHS B
        B.setNeighbour(C, EAST);
        B.setNeighbour(E, SOUTH);

        //NEIGHS C
        C.setNeighbour(B, Direction.WEST);
        C.setNeighbour(F, SOUTH);

        //NEIGHS F
        F.setNeighbour(C, Direction.NORTH);
        F.setNeighbour(I, SOUTH);

        //NEIGHS I
        I.setNeighbour(F, Direction.NORTH);
        I.setNeighbour(H, Direction.WEST);

        //NEIGHS H
        H.setNeighbour(E, Direction.NORTH);
        H.setNeighbour(G, Direction.WEST);
        H.setNeighbour(I, EAST);

        //NEIGHS G
        G.setNeighbour(H, EAST);

    }


    @Test
    public void test_to_graph_node_must_convert_a_game_square2d_node_to_corresponding_graph_node() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        GameSquare2DAdapter adapter = new GameSquare2DAdapter();

        Method m = GameSquare2DAdapter.class.getDeclaredMethod("toGraphNode", GameSquare2D.class);
        m.setAccessible(true);

        GraphNodeImpl convertedA = (GraphNodeImpl) m.invoke(adapter, A);

        Assert.assertTrue(convertedA.getName().equals(A.getName()));

     }


    @Test
    public void test_traverse_must_traverse_all_nodes_in_graph() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        GameSquare2DAdapter adapter = new GameSquare2DAdapter();

        Method m = GameSquare2DAdapter.class.getDeclaredMethod("traverse", GameSquare2D.class);
        m.setAccessible(true);

        m.invoke(adapter, A);

        String visited = adapter.visited
                .stream()
                .map(n -> n.getName())
                .reduce("", String::concat);

        String converted = adapter.converted
                .stream()
                .map(n -> n.getName())
                .reduce("", String::concat);

        System.out.println(visited);

        Assert.assertEquals(visited, converted);

    }

    @Test
    public void test_after_traversing_positions_neighs_should_return_the_correct_indices_of_connecting_neighbours()throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        GameSquare2DAdapter adapter = new GameSquare2DAdapter();

        Method traverse = GameSquare2DAdapter.class.getDeclaredMethod("traverse", GameSquare2D.class);
        traverse.setAccessible(true);

        traverse.invoke(adapter, A);
        Method positionsNeighs = GameSquare2DAdapter.class.getDeclaredMethod("positionsNeighs", GameSquare2D.class);
        positionsNeighs.setAccessible(true);

        Set<Integer> indices = new HashSet<>((List<Integer>) positionsNeighs.invoke(adapter, E));

        Set<Integer> expected = new HashSet<>(Arrays.asList(1, 3, 7 ));


        Assert.assertEquals(expected, indices);

    }

    @Test
    public void test_convert_should_right() throws NoPathPossibleException
    {
        final GameSquare2DAdapter adapter = new GameSquare2DAdapter();
        final Tuple4<GraphNode, GraphNode, List<GameSquare2D>, List<GraphNode>> convert = adapter.convert(A, I);
        final GraphNode start = convert._1;
        final GraphNode end = convert._2;


        Assert.assertTrue(start.getName().equals(A.getName()));
        Assert.assertTrue(end.getName().equals(I.getName()));
        Assert.assertTrue(start.neighboursWithDistances().size() == A.neighboursWithDirections().size());
        Assert.assertTrue(end.neighboursWithDistances().size() == I.neighboursWithDirections().size());


    }


    @Test
    public void test_shortest_path_directions_should_return_the_correct_path() throws NoPathPossibleException, StartAndEndAreThereSameException {

        final DijkstrasAlgorithm dijkstrasAlgorithm= new DijkstrasAlgorithm();
        final List<Direction> directions = dijkstrasAlgorithm.directionsToMove(A, I);
        final List<Direction> directionsExpected = new ArrayList<>(Arrays.asList(SOUTH, EAST, SOUTH, EAST));

        Assert.assertEquals(directionsExpected,directions);


    }

    @Test(expected = StartAndEndAreThereSameException.class)
    public void test_shortest_path_directions_with_same_start_and_end_point_should_throw_exception() throws NoPathPossibleException, StartAndEndAreThereSameException {

        final DijkstrasAlgorithm dijkstrasAlgorithm= new DijkstrasAlgorithm();
        dijkstrasAlgorithm.directionsToMove(A, A);
    }

    @Test(expected = NoPathPossibleException.class)
    public void test_shortest_path_directions_when_no_path_is_possible_an_no_path_possible_exception_should_be_thrown() throws NoPathPossibleException, StartAndEndAreThereSameException {

        final DijkstrasAlgorithm dijkstrasAlgorithm= new DijkstrasAlgorithm();
        dijkstrasAlgorithm.directionsToMove(A, Z);

    }
}