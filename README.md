# Opdracht SE2 deeltijd, Haagse Hogeschool 
 
Dit is een library die gebruikt moet worden voor de opdracht van het vak SE2 Prototyping. 
Bestudeer deze library goed voordat je hiermee daadwerkelijk aan de slag gaat. Het maken van een klassendiagram kan hier bij helpen.

Om de library te gebruiken moet je een aantal libaries op het classpath plaatsen. De libraries zijn [hier](https://bitbucket.org/rplruijs/shortest-path-algorithms/downloads/libraries.zip) als een zip te downloaden.

#BASISSPEL

De Happer game is een variant op het het bekende spel Pacman. Pacman is een klassiek computerspel dat voor het eerst werd uitgebracht in 1980 als een arcadespel. De Happer game wordt gespeeld op een twee dimensionaal rechthoekig speelveld. Dit speelveld bestaat uit N * M vakjes. Een vakje heeft minimaal 2 en maximaal 4 buren. Binnen het spel bevinden zich  één Happer, één Mens en meerdere` dozen en blokken. De Mens is te besturen door de speler van het spel door middel van bijvoorbeeld de pijltjes toetsen. Het is niet mogelijk om diagonaal over het bord te bewegen. Alleen de Mens is in staat om één of meerdere dozen te verplaatsen door er tegen aan te lopen. Blokken zijn niet verplaatsbaar en blijven dus op hun oorspronkelijke positie gedurende het spel. De Happer wordt bestuurd door de computer. Om de n seconden beweegt de Happer zich richting de Mens. De computer maakt gebruik van het kortste pad algoritme om te bepalen in welke richting hij zich op moet bewegen. Dit kortste pad algoritme hoeven jullie niet zelf te schrijven, jullie gaan namelijk gebruik maken van de gegeven library. 
Het doel van het spel is om de Happer in de sluiten door op een slimme manier met de dozen te schuiven. Wanneer de Happer is ingesloten heb je het spel gewonnen. Het aantal stappen dan de Mens uitvoert moet worden bijgehouden en getoond worden wanneer het spel gewonnen is. Het is uiteraard ook mogelijk om het spel te verliezen. Dit gebeurt op het moment dat Happer zich naar een vakje beweegt waar de mens zich bevindt of wanneer de Mens een aantal (door jullie te bepalen) stappen heeft gezet.

Nadat de applicatie opgestart is moet het spel na een aantal seconden starten. Het moet mogelijk zijn om het spel te pauzeren en te resetten. Op het moment dat het spel gepauzeerd wordt, stopt de Happer met bewegen en is het niet mogelijk om de Mens te bewegen. Wanneer het spel de status gepauzeerd heeft moet het ook weer mogelijk zijn om het spel te hervatten. Wanneer een spel gereset wordt, dan worden alle spelelementen teruggezet naar zijn of haar oorspronkelijke positie en wordt het spel na een aantal seconde automatisch weer gestart.


#ONDERZOEK

Jullie bepalen zelf de doelgroep voor jullie Happer spel. Voorbeelden van doelgroepen zijn: alleenstaande gepensioneerde of kinderen met de leeftijd tussen de 6 en 10 jaar.  De globale werking van het spel is hierboven uitgelegd. Het is aan jullie de taak om onderzoek te doen hoe bepaalde usabilty aspecten het beste kunnen worden ingevuld voor jullie doelgroep. Vragen die bij het invullen van deze aspecten gesteld kunnen worden zijn, hoe groot moet het speelveld zijn? hoe moet de mens bestuurd worden? om de hoeveel seconden beweegt de Happer zich? Na hoeveel seconden wordt het spel automatisch gestart.? Hoe worden de spelelementen op het scherm getoond (figuren of afbeeldingen, kleuren ). Op welke posities worden de spelelementen initieel geplaatst. Hoe wordt het spel uitgelegd aan de speler? Het betreft hier dus een onderzoek naar de usability van het spel voor een betreffende doelgroep.


#UITBREIDINGEN

We gaan het spel iets lastiger maken door meerdere levels te introduceren. Het minimaal aantal levels is 3. De levels moeten oplopend in moeilijkheidsgraad zijn. Bedankt zelf hoe je dat kan bereiken.

We introduceren een nieuw Spelelement de Joker. Wanneer de Mens de Joker opeet wordt het aantal levens met 1 opgehoogd. Wanneer de Happer de joker opeet wordt het aantal levens met 1 verlaagd; In beide gevallen wordt er een nieuwe Joker op een willekeurig leeg speelveld geplaatst.

Verzin zelf een uitbreiding op het spel die voor jouw doelgroep interessant is.


